# ROS2 Fedora Images

This folder container several ROS2 container images files, it is currently split into:

* `ros2-src`: An image that contains the ROS2 source code, used by other images as the base one;
* `ros2-core`: build ROS2 `ros_core` variant;
* `ros2-toolbox`: an image to be used with the toolbox CLI, it builds the whole ROS2 stack.

Image are pushed to `quay.io/fedora-sig-robotics/$image:latest`.

## License

[Apache-2.0](../LICENSE)
