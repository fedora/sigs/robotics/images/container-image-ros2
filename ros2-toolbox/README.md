# ROS2 Core Image

This image builds a container image with all ROS2 rolling packages (as much as it can).

It uses the [/ros-src](../ros2-src) as the base image.

This image is supposed to be used as a development environment for ROS2 applications
with [toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/) but it can also be used on its own.

## Building

```
podman build \
-t localhost/ros2-toolbox:latest 
```

## License

[Apache-2.0](../LICENSE)
