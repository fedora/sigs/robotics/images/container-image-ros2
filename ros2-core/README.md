# ROS2 Core Image

This image builds a container image with the `ros_core` flavor package.

It uses the [/ros-src](../ros2-src) as the base image.

## Building

```
podman build \
-t localhost/ros2-core:latest 
```

## License

[Apache-2.0](../LICENSE)
