# ROS2 Src Image

This image contains all the base dependencies and the ROS2 rolling source code.

It is supposed to serve as a starting point to build ROS2 flavors/packages.

## Building

```
podman build \
-t localhost/ros2-src:latest 
```

## License

[Apache-2.0](../LICENSE)
